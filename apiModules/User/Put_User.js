const {
    coreRequest
} = require('../../common/coreRequest');

const Put_User = testData =>
    new Promise((resolve, reject) => {

        let apiService = "/user/";
        let _url = apiService;

        configs = {
            method: "put",
            baseURL: testData.BASEURL,
            url: _url,
            data: testData.user_data
        }

        coreRequest(configs)
            .then(function(response) {
                resolve(response);
            })
            .catch(function(error) {
                reject(error);
                console.log("Error Inside Put_User module: " + error);
            });
    });

module.exports = {
    Put_User
};