const expect = require("chai").expect;
const {
    Post_User
} = require("../apiModules/User/Post_User");
const util = require('util')


var test = {
    data: [{
            BASEURL: "https://supervillain.herokuapp.com/v1/",
            user_data: {
                "username": "testUser" + Math.random(),
                "score": 10
            }
        },
        {
            BASEURL: "https://supervillain.herokuapp.com/v1/",
            user_data: {
                "username": "PutUser",
                "score": 10
            }
        }
    ]
}

describe('User POST API - Test Suite', function() {
    this.timeout(30000);

    describe('POST User Data Check Tests', function() {

        it('Verify \'POST User\' Api response is OK', async function() {
            currentResponseBody = await Post_User(test.data[0]);
            console.log("currentResponse " + currentResponseBody.data);
            expect(currentResponseBody.status).to.equal(201);
            expect(currentResponseBody.data.status).not.to.be.null;
            expect(currentResponseBody.data.message).not.to.be.null;
            expect(currentResponseBody.data.status).to.be.equal("success");
            expect(currentResponseBody.data.message).to.be.equal('User added.');
        });
        it('Add \'User\' for PUT and DELETE Api response', async function() {
            currentResponseBody = await Post_User(test.data[1]);
            console.log("currentResponse " + currentResponseBody.data);
            expect(currentResponseBody.status).to.equal(201);
            expect(currentResponseBody.data.status).not.to.be.null;
            expect(currentResponseBody.data.message).not.to.be.null;
            expect(currentResponseBody.data.status).to.be.equal("success");
            expect(currentResponseBody.data.message).to.be.equal('User added.');
        });
    });
});