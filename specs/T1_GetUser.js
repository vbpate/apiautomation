const expect = require("chai").expect;
const {
    Get_User
} = require("../apiModules/User/Get_User");
const util = require('util')


var test = {
    data: {
        BASEURL: "https://supervillain.herokuapp.com/v1/"
    }
}

describe('User Data API - Test Suite', function() {
    this.timeout(30000);

    describe('Basic Data Check Tests', function() {

        it('Verify \'GET User\' Api response is OK', async function() {
            currentResponseBody = await Get_User(test.data);
            console.log("currentResponse " + currentResponseBody.data);
            expect(currentResponseBody.status).to.equal(200);
            expect(currentResponseBody.data).to.be.an('array');
            for (var i = 0; i < currentResponseBody.data.length; i++) {
                var userObj = currentResponseBody.data[i];
                expect(userObj).to.include.keys('user_id', 'username', 'score');
                expect(userObj.user_id).not.to.be.null;
                expect(userObj.username).not.to.be.null;
                expect(userObj.score).not.to.be.null;
            }
        });
    });
});