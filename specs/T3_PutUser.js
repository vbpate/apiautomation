const expect = require("chai").expect;
const {
    Put_User
} = require("../apiModules/User/Put_User");
const util = require('util')

var test = {
    data: [{
        BASEURL: "https://supervillain.herokuapp.com/v1/",
        user_data: {
            "username": "PutUser",
            "score": 120
        }
    }, {
        BASEURL: "https://supervillain.herokuapp.com/v1/",
        user_data: {
            "usename": "PutUser",
            "scre": 10
        }
    }]
}

describe('User PUT API - Test Suite', function() {
    this.timeout(30000);

    describe('PUT User Data Check Tests', function() {

        it('Verify \'PUT User\' Api response is OK', async function() {
            currentResponseBody = await Put_User(test.data[0]);
            console.log("currentResponse " + currentResponseBody.data);
            expect(currentResponseBody.status).to.be.equal(204);
        });
    });
});

describe('Negative User PUT Request API - Test Suite', function() {
    this.timeout(30000);

    describe('PUT Invalid User Data Check Tests', function() {
        it('Verify \'PUT Invalid User Data\' Api response is OK', async function() {
            currentResponseBody = await Put_User(test.data[1]);
            console.log("currentResponse " + currentResponseBody.data);
            expect(currentResponseBody.status).not.to.be.equal(201);
            expect(currentResponseBody.data.status).to.be.null;
            expect(currentResponseBody.data.message).to.be.null;

        });
    });
});